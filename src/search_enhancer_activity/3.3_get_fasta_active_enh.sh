#!/bin/bash

source constants.sh

IN_DIR=${OUT_DATA_DIR}/enh_active/active_regions
OUT_DIR_FASTA=${IN_DIR}/fasta
OUT_DIR_FASTQ=${IN_DIR}/fake_fastq
MM9_FASTA=${WORK_DIR}/db/all_chr.fa.masked
FASTA_TO_FASTQ=${WORK_DIR}/fasta_to_fastq.pl

[ -d ${IN_DIR} ] || mkdir ${IN_DIR}
[ -d ${OUT_DIR_FASTA} ] || mkdir ${OUT_DIR_FASTA}
[ -d ${OUT_DIR_FASTQ} ] || mkdir ${OUT_DIR_FASTQ}

for res_per_mirna in ${IN_DIR}/*_active_regions.bed
do
	FILE_NAME=${res_per_mirna##*/}
	mirna_id="$( cut -d '_' -f 1 <<< "$FILE_NAME" )"
	# fix error: get coordinates from active_regions folder to convert to fasta
	bedtools getfasta -fi ${MM9_FASTA} -bed ${res_per_mirna} \
					  -fo ${OUT_DIR_FASTA}/${mirna_id}_active_regions.fasta
	perl ${FASTA_TO_FASTQ} \
		 ${OUT_DIR_FASTA}/${mirna_id}_active_regions.fasta \
		 > ${OUT_DIR_FASTQ}/${mirna_id}_active_regions.fastq
done