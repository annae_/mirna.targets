# call as Rscript name ${OUT_DATA_DIR}

#source('src/search_enhancer_activity/func.R')
source('func.R')
suppressMessages(library(stringr))


data.dir <- commandArgs(T)[1]
method.enh <- commandArgs(T)[2]
transcripts <- commandArgs(T)[3]
print(paste0('Transcripts are treated this way: ', transcripts))

#source('src/search_enhancer_activity/constants.R')
#data.dir <- '../genome/out_M1'
#data.dir <- '../genome/out'
source('constants.R')
print(gene.expr.all.path)

setwd(repo.path)
load('data/enh.gene.assoc.sign.RData')

if(method.enh == 'seeds'){
  enh.filtered <- ReadFilesPerMirna(dir = file.path(data.dir, 'enh_with_seeds'))
  
  enh.filtered <- ConvertToDt(enh.filtered)
  setnames(enh.filtered,
           c('V1', 'V2', 'V3'),
           c('enh.chr', 'enh.start', 'enh.end'))
}else if(method.enh == 'miranda'){
  enh.miranda.path <- file.path(data.dir, 'enh_miranda/miranda.enh.tsv')
  enh.filtered <- fread(enh.miranda.path)
  enh.filtered <- unique(enh.filtered[,-c('max.energy', 'max.score')])
}else if(method.enh == 'triplexator'){
  enh.triplexator.path <- file.path(data.dir, 'enh_triplexator/triplex_search.summary')
  enh.filtered <- fread(enh.triplexator.path)[,-c('V11')]
  enh.filtered <- PrepareTriplexatorOutput(dt = enh.filtered)
}


# check if start values are unique
# table(match(enh.filtered$enh.start, enh.gene.assoc.sign$enh.start) == match(enh.filtered$enh.end, enh.gene.assoc.sign$enh.end))

enh.found <- unique(enh.gene.assoc.sign[match(enh.filtered$enh.start, enh.gene.assoc.sign$enh.start)][, c('Enhancer')])
enh.found.genes <- unique(enh.gene.assoc.sign[Enhancer %in% enh.found$Enhancer][`P-value`<alpha])
enh.filtered <- merge(enh.found.genes,
                      enh.filtered,
                      by = c('enh.chr', 'enh.start', 'enh.end'),
                      allow.cartesian=T
                      )[,c('mirna', 'enh.chr', 'enh.start', 'enh.end', 'Gene.Name', 'Enhancer', 'Corr..Coef', 'P-value')]
setnames(enh.filtered,
         c('Gene.Name', 'Enhancer', 'Corr..Coef', 'P-value'),
         c("gene", "enhancer.mm10", "corr(enh, gene)", "p-value"))

# sapply(unique(enh.filtered$mirna), function(mirna.i){
#   subset <- unique(enh.filtered[mirna == mirna.i][, c('gene', 'enhancer.mm10')])
#   dups <- subset[duplicated(subset[, enhancer.mm10]) | duplicated(subset[, enhancer.mm10], fromLast=TRUE)]
#   dups
# }, USE.NAMES = T, simplify = F)

gene.expr.all <- fread(gene.expr.all.path)
load(paste0('data/', type, 'degs.unique.RData'))
gene.expr.all <- GetDEGenesExpr(gene.expr.all,
                                degs)
associated.genes.expr <- enh.filtered[, gene.expr.all[Symbol %in% .SD$gene], by=mirna]
de.mirna.expr <- GetDEMirnasExpr(de.mirnas = enh.filtered$mirna, 
                                 gene.expr.all,
                                 transcripts=transcripts)

corrs.all <- CalcCorrsAllMirnasVsAllGenes(genes.expr = associated.genes.expr,
                                             de.mirna.expr, 
                                             alpha,
                                             gene.column = 'gene',
                                             transcripts=transcripts)
if(transcripts == 'separate'){
  unique(corrs.all[, c('mirna','transcript')])[,.N, by=mirna][order(-N)]
}
unique(corrs.all[, c('mirna','symbol')])[,.N,by=mirna][order(-N)]

enh.filtered <- GetSignifCorrsAllMirnasVsAllGenes(trio.dt = enh.filtered,
                                                 alpha,
                                                 gene.column = 'gene',
                                                 corrs = corrs.all)

all.corrs <- PrepareAllCorrsTable(dt = enh.filtered)
unique(all.corrs[, c('mirna','gene')])[,.N, by=mirna][order(-N)]

# sapply(unique(enh.filtered$mirna), function(mirna.i){
#   subset <- enh.filtered[mirna == mirna.i]
#   x <- subset[duplicated(subset[, enhancer.mm10]) | duplicated(subset[, enhancer.mm10], fromLast=TRUE)]
#   setorder(x, enhancer.mm10)
#   unique(x[, c('enhancer.mm10', 'gene')])
# }, USE.NAMES = T, simplify = F)

save(enh.filtered,
     file = paste0('out/', type, 'enh.filtered.RData'))
save(all.corrs,
     file = paste0('data/', type, 'all.corrs_',  method.enh,  '_transcripts_',  transcripts, '.RData'))
write.table(enh.filtered, 
            file = paste0('out/', type, 'enh.filtered.tsv'),
            quote = F,
            col.names = F,
            row.names = F,
            sep = '\t')
stats.genes <- GetStatsPerMirna(corrs.table = enh.filtered, 
                                genes.column = 'gene')
# print(stats.genes)
