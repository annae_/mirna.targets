#!/bin/bash

source constants.sh

METHODS='seeds miranda triplexator'
METHOD_ENH=$(echo ${METHODS} | cut -d " " -f3)

TRANSCRIPTS_TREAT='sum separate'
TRANSCRIPTS=$(echo ${TRANSCRIPTS_TREAT} | cut -d " " -f2)

GET_ENH=true
GET_SUBSTR_POS=true1 # time-costing step
GET_ENH_WITH_SEEDS=true1

FIND_CORR_TRIOS=true

echo 'method to choose enhancers: '${METHOD_ENH}
if [ ${GET_ENH} = true ]; then
## three methods to choose enhancers
	if [ ${METHOD_ENH} = 'seeds' ]; then

		if [ ${GET_SUBSTR_POS} = true ]; then
			source 1a.1_get_substr_pos.sh
		fi

		if [ ${GET_ENH_WITH_SEEDS} = true ]; then
			echo '1a.2. get enhancers with seeds'
			source 1a.2_get_enh_with_seeds.sh
		fi

	elif [ ${METHOD_ENH} = 'miranda' ]; then

		echo '1b.1_run_miranda'
		bash 1b.1_run_miranda.sh

		echo '1b.2_parse_miranda_output'
		python 1b.2_parse_miranda_output.py ${OUT_DATA_DIR}

	elif [ ${METHOD_ENH} = 'triplexator' ]; then
		echo '1c.1_run_triplexator'
		bash 1c.1_run_triplexator.sh
	else
		echo "select the way to filter enhancers"
	fi
fi

## find correlations for trios
if [ ${FIND_CORR_TRIOS} = true ]; then
	echo '2. find correlated trios'
	Rscript 2_find_corr_trios.R ${OUT_DATA_DIR} ${METHOD_ENH} ${TRANSCRIPTS}
fi

## post finding correlations
if [ ${METHOD_ENH} = 'seeds' ]; then

	echo '3.1. make bed with active enhancers'
	Rscript 3.1_make_bed_enh_active.R ${OUT_DATA_DIR} ${TRANSCRIPTS}

	echo '3.2. get active regions'
	if [ ${METHOD_ENH} = 'seeds' ]; then 
		interval_plus_minus_to_seed=14
	elif [ ${METHOD_ENH} = 'miranda' ]; then 
		interval_plus_minus_to_seed=0
	fi
	bash 3.2_get_active_regions.sh ${interval_plus_minus_to_seed}

	echo '3.3. get fasta of active enhancers'
	bash 3.3_get_fasta_active_enh.sh

	echo '3.4. sort fasta of active enhancers by seed'
	python 3.4_sort_fasta_by_seed.py ${WORK_DIR} ${OUT_DATA_DIR} ${TYPE}

	echo '3.5. align needle'
	if [ ${METHOD_ENH} = 'seeds' ]; then 
		mirna_extent='mature'
	elif [ ${METHOD_ENH} = 'miranda' ]; then 
		mirna_extent='seed'
	fi
	python 3.5_align_needle.py ${WORK_DIR} ${OUT_DATA_DIR} ${mirna_extent} ${TYPE}
	
	echo '3.6. get best aligned'
	Rscript 3.6_get_best_aligned.R ${WORK_DIR} ${OUT_DATA_DIR} ${TRANSCRIPTS}

	echo '3.7. get best aligned enhancers'
	bash 3.7_get_enh_best_aligned.sh

	echo '3.8. get final table'
	Rscript 3.8_get_final_table.R ${WORK_DIR} ${OUT_DATA_DIR} \
								  ${TRANSCRIPTS} ${METHOD_ENH}
fi

echo 'finished'
