#!/bin/bash


WORK_DIR=/mnt/storage2/annae/macrophages/genome

ENH_WITH_SEED_DIR=${WORK_DIR}/out/enh_with_seeds

ENHANCERS=${WORK_DIR}/mouse_permissive_enhancers_phase_1_and_2.bed


echo "mirna_id enhancers seeds"

for enh_with_seed in ${ENH_WITH_SEED_DIR}/*enh_with_seeds.bed

do

	FILE_NAME=${enh_with_seed##*/}
 
	mirna_id="$( cut -d '_' -f 1 <<< "$FILE_NAME" )"

	echo ${mirna_id} | tr '\n' ' '

	less ${enh_with_seed} | wc -l | tr '\n' ' '
	LC_NUMERIC="en_US.UTF-8" printf '%.*f\n' 3 \
		$(echo $(less ${enh_with_seed} | wc -l)/$(less ${ENHANCERS} | wc -l) | bc -l) | tr '\n' ' '

	LC_NUMERIC="en_US.UTF-8" printf '%.*f\n' 4 \
		$(echo $(less ${enh_with_seed} | wc -l)/ \
		$(less ${WORK_DIR}/out/${mirna_id}_seed_pos_in_genome | wc -l) | bc -l) | tr '\n' ' '

	seed_pos_in_genome=${WORK_DIR}/out/${mirna_id}_seed_pos_in_genome

	less ${seed_pos_in_genome} | wc -l
	

done