#!/bin/bash

source constants.sh
IN_DIR=${OUT_DATA_DIR}/enh_active/active_regions/fasta
OUT_DIR_FASTA=${IN_DIR}/fasta_sorted+mirna
ALIGN_HTML_DIR=${OUT_DATA_DIR}/alignments_MUSCLE/html
ALIGN_MSF_DIR=${OUT_DATA_DIR}/alignments_MUSCLE/msf


IN_DIR_FASTA=${IN_DIR}/fasta_sorted
MIRNAS_PATH=${WORK_DIR}/mirnas/mature_seqs
MIRNAS_SEED_FASTA_PATH=${WORK_DIR}/mirnas/seeds_fasta
MUSCLE_PATH=${WORK_DIR}/tools/muscle3.8.31_i86linux64

[ -d ${OUT_DIR_FASTA} ] || mkdir ${OUT_DIR_FASTA}
[ -d ${ALIGN_HTML_DIR} ] || mkdir ${ALIGN_HTML_DIR}
[ -d ${ALIGN_MSF_DIR} ] || mkdir ${ALIGN_MSF_DIR}

for mirna in ${MIRNAS_PATH}/*
do
	FILE_NAME=${mirna##*/}
	echo ${FILE_NAME}
	if [ -f ${IN_DIR_FASTA}/${FILE_NAME} ]
	then
		cat ${mirna} ${IN_DIR_FASTA}/${FILE_NAME} ${MIRNAS_SEED_FASTA_PATH}/${FILE_NAME} \
			> ${OUT_DIR_FASTA}/${FILE_NAME}
	fi
done

for mirna in ${OUT_DIR_FASTA}/*
do
	FILE_NAME=${mirna##*/}
	${MUSCLE_PATH} -in ${mirna} -html -out ${ALIGN_HTML_DIR}/${FILE_NAME}_muscle.html
	${MUSCLE_PATH} -in ${mirna} -clw -out ${ALIGN_MSF_DIR}/${FILE_NAME}_muscle.clw
done