# call as python name ${WORK_DIR} ${OUT_DATA_DIR}

import shutil, os, sys
from Bio import SeqIO

#work_dir = '/home/anna/biengi/genome/'
#work_dir = '/mnt/storage2/annae/macrophages/genome'

#out_data_dir = os.path.join(work_dir, 'out')
work_dir = sys.argv[1]
out_data_dir = sys.argv[2]
type_macrophages = sys.argv[3]
active_regions_path = os.path.join(out_data_dir, 'enh_active/active_regions/fasta')
out_dir = os.path.join(active_regions_path, 'fasta_sorted')
seeds_forward_path =os.path.join(work_dir, 'seed_seq_full_'+type_macrophages)
seeds_rev_compl_path = os.path.join(work_dir, 'seed_seq_reverse_compl_full_'+type_macrophages)

seeds_dict = dict()

i = 0
with open(seeds_forward_path) as seeds_forward:
    for seed in seeds_forward:
        mirna_num = seed.split()[0]
        print(mirna_num)
        mirna_seed_seq = seed.split()[1]
        if mirna_num != '146b-5p':
            seeds_dict[mirna_num+'_'+str(i)] = mirna_seed_seq
            i+=1
            
i = 0
with open(seeds_rev_compl_path) as seeds_rev_compl:
    for seed in seeds_rev_compl:
        mirna_num = seed.split()[0]
        print(mirna_num)
        mirna_seed_seq = seed.split()[1]
        if mirna_num != '146b-5p':
            seeds_dict[mirna_num+ '_rev_compl_'+str(i)] = mirna_seed_seq
            i+=1

if os.path.exists(out_dir) and os.path.isdir(out_dir):
    shutil.rmtree(out_dir)
os.makedirs(out_dir)

for filename in os.listdir(active_regions_path):
    file_path = os.path.join(active_regions_path, filename)
    if os.path.isfile(file_path):
        print(filename)
        for record in SeqIO.parse(file_path, "fasta"):
            #print record.id
            i = 0
            mirna_filename = filename.split('_')[0]
            #print(mirna_filename)
            for mirna_name, mirna_seed in seeds_dict.items():
                mirna_name_num = filter(str.isdigit, mirna_name.split('_')[0].split('-')[0])
                if mirna_name_num == mirna_filename and mirna_seed in record.seq:
                    mir_name = "_".join(mirna_name.split('_')[:-1])
                    #print mirna_seed, record.seq, mir_name
                    
                    output = open(os.path.join(out_dir, mir_name),'a') 
                    output.write(">%s\n%s\n"%(record.id, record.seq))
                    output.close()
