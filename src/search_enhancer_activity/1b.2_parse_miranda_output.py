import os, sys, pandas as pd

out_data_dir = sys.argv[1]
# out_data_dir = '/mnt/storage2/annae/macrophages/genome/out/'
out_dir = os.path.join(out_data_dir, 'enh_miranda')
target_text = '>>'
file_path = os.path.join(out_dir,'miranda.out')
take_next_line = False
res_df = pd.DataFrame()

with open(file_path) as lines:
    for line in lines:
        if target_text in line:
#             print line.split()[4]
            mirna = 'Mir' + filter(lambda x: x.isdigit(), line.split()[0].split('-')[2])
            enh_chr = line.split()[1].split(':')[0]
            enh_start = line.split()[1].split(':')[1].split('-')[0]
            enh_end = line.split()[1].split(':')[1].split('-')[1]
            max_score = line.split()[4]
            max_energy = line.split()[5]
            res_new = pd.DataFrame({'mirna': [mirna],
                                   'enh.chr': [enh_chr],
                                   'enh.start': [enh_start],
                                   'enh.end': [enh_end],
                                   'max.score': [max_score],
                                   'max.energy': [max_energy] 
                                  })
            res_df = res_df.append(res_new, ignore_index = True)
        
print res_df
res_df.to_csv(os.path.join(out_dir, 'miranda.enh.tsv'), 
              sep = '\t',
              index = False)