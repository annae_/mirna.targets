#!/bin/bash

source constants.sh

ENH_WITH_SEED_DIR=${OUT_DATA_DIR}/enh_with_seeds
ANNO=${WORK_DIR}/db/mm9.annotation_sorted.bed
OUT_DIR=${OUT_DATA_DIR}/closest_genes

[ -d ${OUT_DIR} ] || mkdir ${OUT_DIR}

for enh_with_seed in ${ENH_WITH_SEED_DIR}/*enh_with_seeds.bed
do
	FILE_NAME=${enh_with_seed##*/}
	mirna_id="$( cut -d '_' -f 1 <<< "$FILE_NAME" )"
	echo ${mirna_id} 
	bedtools closest -a ${enh_with_seed} -b ${ANNO} -d -t all | awk '($25)<10000' \
		> ${OUT_DIR}/${mirna_id}_genes_closest_to_enh_with_seeds.bed
done


