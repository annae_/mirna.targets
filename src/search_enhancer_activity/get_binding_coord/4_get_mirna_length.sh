TYPE_OF_DATA=non-stimulated.BMDMs

MAIN_PATH=/mnt/storage2/annae/macrophages
WORK_DIR=${MAIN_PATH}/genome
mirnas_fasta=${WORK_DIR}/mirnas/DE_mirna_mature_seqs_forward.fa
OUT_DATA_DIR=${MAIN_PATH}/mirna.targets/out/binding_sites_coords/${TYPE_OF_DATA}

rm -f ${OUT_DATA_DIR}/mirnas_lengths

while read line
do
    if [[ $line =~ ">" ]]
    then
    	mirna="${line/>mmu-miR-/''}"
    	echo $mirna
    else
    	length=${#line}
       	paste  <(printf %s "$mirna") <(printf %s "$length") \
			>> ${OUT_DATA_DIR}/mirnas_lengths
    fi
done < $mirnas_fasta