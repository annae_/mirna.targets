
TYPE_OF_DATA=M2
MAIN_PATH=/mnt/storage2/annae/macrophages
MM10_FASTA=$MAIN_PATH/genome/db.mm10/unmasked/all_chr.fa
OUT_DIR=${MAIN_PATH}/mirna.targets/out/binding_sites_coords/${TYPE_OF_DATA}

for res_per_mirna in ${OUT_DIR}/*enh.bed
do
	fname=$(basename "$res_per_mirna" .txt)
	echo $fname
	bedtools getfasta -fi ${MM10_FASTA} -bed ${res_per_mirna} -fo $OUT_DIR/${fname}.fasta
done