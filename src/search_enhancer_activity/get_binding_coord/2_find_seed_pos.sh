#!/bin/bash

TYPE_OF_DATA=M2
# non-stimulated.BMDMs
mirnas="1956"
# "22 155 210 221 222"

MAIN_PATH=/mnt/storage2/annae/macrophages
WORK_DIR=${MAIN_PATH}/genome
ENH_FASTAS=~/*_enh.bed.fasta
FORWARD_SEEDS=$WORK_DIR/seed_seq_full_M2
REVERSE_COMPL_SEEDS=$WORK_DIR/seed_seq_reverse_compl_full_M2
OUT_DATA_DIR=${MAIN_PATH}/mirna.targets/out/binding_sites_coords/${TYPE_OF_DATA}

# test db and seeds:
# DB_PATH=$WORK_DIR/test_db/chr*fa
# FORWARD_SEEDS=$WORK_DIR/test_seeds
# REVERSE_COMPL_SEEDS=$WORK_DIR/test_seeds_reverse_compl

[ -d ${OUT_DATA_DIR} ] || mkdir ${OUT_DATA_DIR}


for mirna in ${mirnas}
do
	echo ${mirna}
	rm -f ${OUT_DATA_DIR}/${mirna}_seed_pos_in_enh
	# seeds1=$(grep -P "^"${mirna}'\t' ${REVERSE_COMPL_SEEDS} | cut -f2)
	while read line; do
  		# echo "$line"
  		seed=$(echo "$line" | cut -f2)
  		seed_name=$(echo "$line" | cut -f1)
  		# echo $seed_name
  		seed_mirna=$(echo "$seed_name" | cut -d '-' -f1 | sed 's/[^0-9]*//g')
  		if [ "$mirna" == "$seed_mirna" ]; then
  			FASTA_PATH=${OUT_DATA_DIR}/${mirna}_enh.bed.fasta
	  		while read line
			do
			    if [[ $line =~ ">" ]]
			    then
			        enh=$(echo $line | tr -d ">")
			    else
			        start_pos=$(echo $line | tr -d '\n' | \
			        			grep -aob ${seed} | grep -oE '[0-9]+')
			       	if [ "$start_pos" != "" ]; then
						paste <(printf %s "$enh") <(printf %s "$start_pos") <(printf %s "$seed") <(printf %s "$seed_name") <(printf %s "reverse_compl") \
						>> ${OUT_DATA_DIR}/${mirna}_seed_pos_in_enh
					fi
			    fi
		done < $FASTA_PATH
		fi

		# grep -aob ${seed} | grep -oE '[0-9]+' > ${OUT_DATA_DIR}/${mirna}_tmp.start_positions

		# awk -v prefix="$chr" -v postfix=" ${#seed}" '{print prefix"\t"$0"\t"$0+postfix}' \
		# ${OUT_DATA_DIR}/${mirna}_tmp.start_positions >> ${OUT_DATA_DIR}/${mirna}_seed_pos_in_enh
	done <${REVERSE_COMPL_SEEDS}

	while read line; do
  		# echo "$line"
  		seed=$(echo "$line" | cut -f2)
  		seed_name=$(echo "$line" | cut -f1)
  		# echo $seed_name
		seed_mirna=$(echo "$seed_name" | cut -d '-' -f1 | sed 's/[^0-9]*//g')
  		if [ "$mirna" == "$seed_mirna" ]; then
	  		FASTA_PATH=${OUT_DATA_DIR}/${mirna}_enh.bed.fasta
	  		while read line
			do
			    if [[ $line =~ ">" ]]
			    then
			        enh=$(echo $line | tr -d ">")
			        #echo $enh
			    else
			        start_pos=$(echo $line | tr -d '\n' | \
			        			grep -aob ${seed} | grep -oE '[0-9]+')
			       	if [ "$start_pos" != "" ]; then
						paste <(printf %s "$enh") <(printf %s "$start_pos") <(printf %s "$seed") <(printf %s "$seed_name") <(printf %s "forward") \
						>> ${OUT_DATA_DIR}/${mirna}_seed_pos_in_enh
					fi
			    fi
			done < $FASTA_PATH
		fi

		# grep -aob ${seed} | grep -oE '[0-9]+' > ${OUT_DATA_DIR}/${mirna}_tmp.start_positions

		# awk -v prefix="$chr" -v postfix=" ${#seed}" '{print prefix"\t"$0"\t"$0+postfix}' \
		# ${OUT_DATA_DIR}/${mirna}_tmp.start_positions >> ${OUT_DATA_DIR}/${mirna}_seed_pos_in_enh
	done <${FORWARD_SEEDS}
	rm -f ${OUT_DATA_DIR}/${mirna}_tmp.start_positions
done
