TYPE_OF_DATA=non-stimulated.BMDMs
mirnas="22 155 210 221 222"

MAIN_PATH=/mnt/storage2/annae/macrophages
WORK_DIR=${MAIN_PATH}/genome
FORWARD_SEEDS=$WORK_DIR/seed_seq_full
REVERSE_COMPL_SEEDS=$WORK_DIR/seed_seq_reverse_compl_full
MIRNA_FASTAS_DIR=$WORK_DIR/mirnas/mature_seqs
OUT_DATA_DIR=${MAIN_PATH}/mirna.targets/out/binding_sites_coords/${TYPE_OF_DATA}



rm -f ${OUT_DATA_DIR}/seed_pos_in_mirnas
while read line; do
		# echo "$line"
		seed=$(echo "$line" | cut -f2)
		seed_name=$(echo "$line" | cut -f1)
		# echo $seed_name
		mirna=$(echo "$seed_name" | cut -d '-' -f1 | sed 's/[^0-9]*//g')


  		while read line
		do
		    if [[ $line =~ ">" ]]
		    then
		    	:
		    else
		        start_pos=$(echo $line | tr -d '\n' | \
		        			grep -aob ${seed} | grep -oE '[0-9]+')
		       	if [ "$start_pos" != "" ]; then
					paste  <(printf %s "$seed_name") \
					<(printf %s "$start_pos") <(printf %s "$seed") \
					>> ${OUT_DATA_DIR}/seed_pos_in_mirnas
				fi
		    fi
		done < $MIRNA_FASTAS_DIR/$seed_name

	# grep -aob ${seed} | grep -oE '[0-9]+' > ${OUT_DATA_DIR}/${mirna}_tmp.start_positions

	# awk -v prefix="$chr" -v postfix=" ${#seed}" '{print prefix"\t"$0"\t"$0+postfix}' \
	# ${OUT_DATA_DIR}/${mirna}_tmp.start_positions >> ${OUT_DATA_DIR}/${mirna}_seed_pos_in_enh
done <${FORWARD_SEEDS}
	
