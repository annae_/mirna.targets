#!/bin/bash

TYPE_OF_DATA=non-stimulated.BMDMs

MAIN_PATH=/mnt/storage2/annae/macrophages
WORK_DIR=${MAIN_PATH}/genome
OUT_DATA_DIR=${MAIN_PATH}/mirna.targets/out/binding_sites_coords/${TYPE_OF_DATA}
MIR_SEQ_PATH=${WORK_DIR}/mirnas/mature_seqs

ENH_PATH=${OUT_DATA_DIR}/*_triplex_enh.bed.fasta

for fasta in ${ENH_PATH}
do
	f="$(basename -- $fasta)"
	mirna="${f/_triplex_enh.bed.fasta/''}"
	echo ${mirna}
	cat ${MIR_SEQ_PATH}/${mirna}-* > ${OUT_DATA_DIR}/${mirna}_mature_seqs.fa
	triplexator -ss ${OUT_DATA_DIR}/${mirna}_mature_seqs.fa -ds $fasta -p 6 --error-rate 19 --lower-length-bound 11 \
				--output-format 0 > ${OUT_DATA_DIR}/${mirna}_triplexator.tsv
done
