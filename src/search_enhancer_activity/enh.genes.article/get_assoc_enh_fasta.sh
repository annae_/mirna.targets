#!/bin/bash

source ../constants.sh

MM9_FASTA=${WORK_DIR}/db/unmasked/all_chr.fa
MM10_FASTA=${WORK_DIR}/db.mm10/all_chr.fa.masked
BED_ASSOC_ENH=${WORK_DIR}/enhancers.mactophages.Mtb.bed

bedtools getfasta -fi ${MM9_FASTA} -bed ${BED_ASSOC_ENH} \
					  -fo ${WORK_DIR}/enhancers.mactophages.Mtb.mm9.fasta