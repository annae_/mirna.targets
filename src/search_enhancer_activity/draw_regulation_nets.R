library(igraph)
library(mem)
library(data.table)
library(prettyGraphs)
source('src/search_enhancer_activity/func.R')

png('out/pics/REG.NETS.FINAL.png', width = 3000, height = 1500, res = 300)
res.dir <- '/home/anna/biengi/mirna.targets/data/'
res.dir <- '/mnt/storage2/annae/macrophages/mirna.targets/data/'
load('data/all.res.list.RData')

common.cols <- c('mirna', 'gene', 'enhancer')


# dt <- unique(merge(res$`seeds|miranda`$`sum+separate.mult.enh`,
#                    res$seeds$separate,
#                    by = common.cols,
#                    all.x=T))

# enh.names <- data.table(enhancer = unique(dt$enhancer))
# enh.names[,enh.num:=paste0('enh.', rownames(enh.names))]
# dt <- merge(dt,
#             enh.names,
#             by = 'enhancer')

long.dt.1 <- data.table(node1 = c('Mir155', 'enh.16', 'enh.16',  'Cxcl1', 'Mir22',
                                'enh.9', 'enh.9', 'Cxcl2'),
                        node2 = c('enh.16', 'Cxcl1', 'Cxcl2', 'CXCR2', 'enh.9',
                                'Cxcl1', 'Cxcl2', 'CXCR2'),
                        weight = rep(0.5, 8))

long.dt.2 <- data.table(node1 = c('Mir222', 'Mir221', 'enh.12', 'enh.12', 'Nfkb1', 'NFKBIA',
                                  'Ube2d3', 'enh.15', 'enh.15', 'Mir155', 'Mir155', 'Mir155',
                                  'enh.3', 'enh.7', 'Malt1', 'Peli1', 'Mir221', 'enh.19',
                                  'Mir22', 'enh.10', 'Mir155', 'enh.14', 'Mir155',
                                  'enh.13', 'TNFAIP3'),
                        node2 = c('enh.12', 'enh.12', 'Nfkb1', 'Ube2d3', 'NF-kB', 'NF-kB',
                                  'NFKBIA','Ube2d3', 'Nfkb1', 'enh.15', 'enh.3', 'enh.7',
                                  'Malt1', 'Malt1', 'NF-kB', 'NF-kB', 'enh.19', 'Peli1',
                                  'enh.10', 'Peli1', 'enh.14', 'Malt1', 'enh.13',
                                  'TNFAIP3', 'NF-kB'),
                        weight = rep(0.8, 25))

long.dt.M2 <- data.table(node1 = c('Mir142', 'Mir142', 'Mir142', 'enh.1', 'enh.2', 'Nfkb1', 'Ube2d3', 'NFKBIA', 'Mir5122', 'enh.3', 'Rela',
                                   'Mir1956', 'Mir1956', 'enh.4', 'Cd14', 'enh.5', 'Tnfaip3', 'Mir1956', 'enh.6', 'TRIF'),
                         node2 = c('enh.1', 'enh.1', 'enh.2', 'Ube2d3', 'Nfkb1', 'NF-kB','NFKBIA', 'NF-kB', 'enh.3', 'Rela', 'NF-kB',
                                   'enh.4', 'enh.5', 'Cd14', 'NF-kB', 'Tnfaip3', 'NF-kB', 'enh.6', 'TRIF', 'NF-kB'),
                         weight = rep(0.8, 20))
# long.dt$weight <- as.numeric(long.dt$weight)

class.names <- list(mirna = 15,
                    gene = 21,
                    out.protein = 26,
                    enh = 18,
                    cell = 22)
classes.long.dt.2<- data.table(name = unique(c('Mir222', 'Mir221', 'enh.12', 'enh.12', 'Nfkb1', 'NFKBIA',
                                               'Ube2d3', 'enh.15', 'enh.15', 'Mir155', 'Mir155', 'Mir155',
                                               'enh.3', 'enh.7', 'Malt1', 'Peli1', 'Mir221', 'enh.19',
                                               'Mir22', 'enh.10', 'Mir155', 'enh.14', 'Mir155',
                                               'enh.13', 'TNFAIP3', 'enh.12', 'enh.12', 'Nfkb1', 'Ube2d3', 'NF-kB', 'NF-kB',
                                               'NFKBIA','Ube2d3', 'Nfkb1', 'enh.15', 'enh.3', 'enh.7',
                                               'Malt1', 'Malt1', 'NF-kB', 'NF-kB', 'enh.19', 'Peli1',
                                               'enh.10', 'Peli1', 'enh.14', 'Malt1', 'enh.13',
                                               'TNFAIP3', 'NF-kB')),
                      class = c(class.names$mirna, class.names$mirna, class.names$enh, class.names$gene, class.names$out.protein, 
                                class.names$gene,
                                class.names$enh, class.names$mirna, class.names$enh, class.names$enh, class.names$gene, 
                                class.names$gene,
                                class.names$enh, class.names$mirna, class.names$enh, 
                                class.names$enh, class.names$enh, class.names$gene, class.names$out.protein))
classes.long.dt.1<- data.table(name = unique(c('Mir155', 'enh.16', 'enh.16',  'Cxcl1', 'CXCR2', 'Mir22',
                                               'enh.9', 'enh.9', 'Cxcl2', 'enh.16', 'Cxcl1', 'Cxcl2', 
                                               'CXCR2', 'enh.9',
                                               'Cxcl1', 'Cxcl2', 'CXCR2', 'Cxcl2')),
                               class = c(class.names$mirna, class.names$enh, class.names$gene, class.names$out.protein, class.names$mirna,
                                         class.names$enh, class.names$gene))

classes.long.dt.M2<- data.table(name = unique(c('Mir142', 'Mir142', 'enh.1', 'enh.2', 'Nfkb1', 'Ube2d3', 'NFKBIA', 'Mir5122', 'enh.3', 'Rela',
                                                'Mir1956', 'Mir1956', 'enh.4', 'Cd14', 'enh.5', 'Tnfaip3', 'enh.1', 'enh.2', 'Ube2d3', 
                                                'Nfkb1', 'NF-kB','NFKBIA', 'NF-kB', 'enh.3', 'Rela', 'NF-kB',
                                                        'enh.4', 'enh.5', 'Cd14', 'NF-kB', 'Tnfaip3', 'NF-kB', 'TRIF', 'enh.6')),
                               class = c(class.names$mirna, class.names$enh, class.names$enh, class.names$gene, class.names$gene, 
                                         class.names$out.protein, class.names$mirna, class.names$enh, class.names$gene, class.names$mirna, 
                                         class.names$enh, class.names$gene, class.names$enh, class.names$gene, class.names$out.protein,
                                         class.names$gene, class.names$enh))

table.net <- long.dt.2
classes <- classes.long.dt.2
# 
# class.names <- c(9,19,8)
# classes <- data.table(name = unique(dt$mirna),
#                       class = class.names[2])
# classes.2 <- data.table(region = unique(dt$enhancer),
#                         class =  class.names[3])
# classes.3 <- data.table(name = unique(dt$gene),
#                         class =  class.names[1])
# classes <- rbindlist(list(classes, classes.2, classes.3))


net <- graph.data.frame(d = table.net, directed=T)

E(net)$width <- sapply(E(net), function(x) {
  table.net[x]$weight
})
E(net)$lty <- sapply(E(net), function(x) {
  'twodash'
  ifelse(table.net[x]$node2 == 'NF-kB' | table.net[x]$node2 == 'NFKBIA',
         'solid',
         'twodash')
})
E(net)$weight <- as.numeric(table.net$weight)^2

V(net)$size <- sapply(V(net)$name, function(x) {
  as.numeric(classes[name == x]$class)
})

V(net)$frame.color <- add.alpha('gray50', 0.5)
V(net)$label.color <- 'gray20'
V(net)$label.cex <- sapply(V(net)$name, function(x) {
  if (classes[name == x]$class ==  class.names$mirna)
    0.7 # mirnas
  else if (classes[name == x]$class ==  class.names$enh)
    0.7 # enh
  else if (classes[name == x]$class ==  class.names$gene)
    0.7 # genes
  else if (classes[name == x]$class ==  class.names$out.protein)
    0.7 # outside protein
  else
    0.5 #cells
})
V(net)$color <- sapply(V(net)$name, function(x) {
  # add.alpha('gold', 0.5)
  if (classes[name == x]$class ==  class.names$mirna)
    'gold'
  else if (classes[name == x]$class ==  class.names$enh)
    'aquamarine3'
  else if (classes[name == x]$class ==  class.names$cell)
    'white'
  else if (classes[name == x]$class ==  class.names$out.protein)
    'white'
  else
    'lemonchiffon'
})
edge.col.dt.2 <- c('gray23', 'gray23', 'olivedrab4', 'olivedrab4', 'olivedrab4', 
                   'red3', 'red3', 'olivedrab4', 'olivedrab4', 'gray65', 
                   'gray23', 'gray23', 'olivedrab4', 'olivedrab4', 'olivedrab4',
                    'red3', 'gray65', 'olivedrab4',
                   'gray23', 'olivedrab4', 'gray65', 'olivedrab4', 'gray65',
                   'olivedrab4', 'red3', 'red3', 'olivedrab4')

edge.col.dt.1 <- c('gray65', 'olivedrab4', 'olivedrab4', 'olivedrab4',
                   'olivedrab4', 'gray23', 'olivedrab4', 'olivedrab4',
                   'olivedrab4', 'red3')

edge.col.dt.M2 <- c('gray23', 'gray65', 'gray65', 'olivedrab4', 'olivedrab4', 'olivedrab4', 'red3', 'red3', 'gray23', 'olivedrab4',
                    'olivedrab4', 'gray23', 'gray23', 'olivedrab4', 'olivedrab4', 'olivedrab4', 'red3', 'gray65', 'olivedrab4', 'olivedrab4')
edge.col <- edge.col.dt.2
  
  
V(net)$label.font <- 1
#am.coord1 <- layout.fruchterman.reingold(net)
# saveRDS(am.coord2, file='out/am.coord2.RDS')
am.coord1 = readRDS('out/am.coord1.RDS')
#NFkBIA:
am.coord1[5,1] <- 7
am.coord1[5,2] <- 20

am.coord1[6,1] <- 7
am.coord1[6,2] <- 15

am.coord1[18,1] <- 2
am.coord1[18,2] <- 23

am.coord1[17,1] <- 0.5
am.coord1[17,2] <- 20
  
am.coord1[11,2] <- 20

am.coord1[8,1] <- 3
am.coord1[8,2] <- 17


#pdf(paste0('out/pics/M2.reg.net.NFKB.pdf'))
par(mfrow = c(1,2), mar=c(0,0,0,0))
plot(net, 
     layout = am.coord1,
     edge.arrow.size=.4,
     edge.color=edge.col)
     #vertex.label.dist =lab.locs)
text(x = -0.9, y = 1.1, 'A', cex = 1.5)




# CXCLS
table.net <- long.dt.1
classes <- classes.long.dt.1
# 
# class.names <- c(9,19,8)
# classes <- data.table(name = unique(dt$mirna),
#                       class = class.names[2])
# classes.2 <- data.table(region = unique(dt$enhancer),
#                         class =  class.names[3])
# classes.3 <- data.table(name = unique(dt$gene),
#                         class =  class.names[1])
# classes <- rbindlist(list(classes, classes.2, classes.3))


net <- graph.data.frame(d = table.net, directed=T)

E(net)$width <- sapply(E(net), function(x) {
  table.net[x]$weight
})

E(net)$lty <- sapply(E(net), function(x) {
  'twodash'
  ifelse(table.net[x]$node2 == 'CXCR2',
         'solid',
         'twodash')
})

E(net)$weight <- as.numeric(table.net$weight)

V(net)$size <- sapply(V(net)$name, function(x) {
  as.numeric(classes[name == x]$class)
})

V(net)$frame.color <- add.alpha('gray50', 0.5)
V(net)$label.color <- 'gray20'
V(net)$label.cex <- sapply(V(net)$name, function(x) {
  if (classes[name == x]$class ==  class.names$mirna)
    0.7 # mirnas
  else if (classes[name == x]$class ==  class.names$enh)
    0.7 # enh
  else if (classes[name == x]$class ==  class.names$gene)
    0.7 # genes
  else if (classes[name == x]$class ==  class.names$out.protein)
    0.7 # outside protein
  else
    0.7 #cells
})
V(net)$color <- sapply(V(net)$name, function(x) {
  # add.alpha('gold', 0.5)
  if (classes[name == x]$class ==  class.names$mirna)
    'gold'
  else if (classes[name == x]$class ==  class.names$enh)
    'aquamarine3'
  else if (classes[name == x]$class ==  class.names$cell)
    'white'
  else if (classes[name == x]$class ==  class.names$out.protein)
    'white'
  else
    'lemonchiffon'
})
edge.col.dt.2 <- c('gray23', 'gray23', 'olivedrab4', 'olivedrab4', 'olivedrab4', 
                   'red3', 'red3', 'olivedrab4', 'olivedrab4', 'gray65', 
                   'gray23', 'gray23', 'olivedrab4', 'olivedrab4', 'olivedrab4',
                   'red3', 'gray65', 'olivedrab4',
                   'gray23', 'olivedrab4', 'gray65', 'olivedrab4', 'gray65',
                   'olivedrab4', 'red3', 'red3', 'olivedrab4')

edge.col.dt.1 <- c('gray65', 
                   'olivedrab4', 'olivedrab4', 'olivedrab4',
                   'gray23', 'olivedrab4', 'olivedrab4',
                   'olivedrab4')

edge.col.dt.M2 <- c('gray23', 'gray65', 'gray65', 'olivedrab4', 'olivedrab4', 'olivedrab4', 'red3', 'red3', 'gray23', 'olivedrab4',
                    'olivedrab4', 'gray23', 'gray23', 'olivedrab4', 'olivedrab4', 'olivedrab4', 'red3', 'gray65', 'olivedrab4', 'olivedrab4')
edge.col <- edge.col.dt.1


V(net)$label.font <- 1
# am.coord2 <- layout.fruchterman.reingold(net)
am.coord2 = readRDS('out/am.coord2.RDS')


plot(net, 
     layout = am.coord2,
     edge.arrow.size=.4,
     edge.color=edge.col)
#vertex.label.dist =lab.locs)
text(x = -0.9, y = 1.1, 'B', cex = 1.5)






legend(x=-1.2, y=-0.8, c("miRNA","enhancer", "target gene", 'key proteins for network function'), pch=21,
       col="#777777", pt.bg=c('gold', 'aquamarine3', 'lemonchiffon', 'white'), pt.cex=1.7, cex=.8, bty="n", ncol=1)
legend(x=0.2, y=-0.8, c("activation","repression", 'duplex formation', 'triplex formation'), pch='-',
       col=c('olivedrab4', 'red3', 'gray23', 'gray65'), pt.cex=3, cex=.8, bty="n", ncol=1)
dev.off()
