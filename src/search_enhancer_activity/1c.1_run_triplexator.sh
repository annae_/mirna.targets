#!/bin/bash

source constants.sh

ENH_PATH=${WORK_DIR}/enhancers.mactophages.Mtb.mm9.fasta
MIR_SEQ_PATH=${WORK_DIR}/mirnas/DE_mirna_mature_seqs_forward_M1.fa
OUT_DIR=${OUT_DATA_DIR}/enh_triplexator

[ -d ${OUT_DIR} ] || mkdir ${OUT_DIR}

#triplexator -ss $MIR_SEQ_PATH -ds $ENH_PATH -p 6 --error-rate 19 --lower-length-bound 11 --output-format 0 > ${OUT_DIR}/result
triplexator -ss $MIR_SEQ_PATH -ds $ENH_PATH -p 6 --error-rate 19 \
			--lower-length-bound 11 -od ${OUT_DIR}

sed -i 's/GT (rel)/GT (rel)	/g' ${OUT_DIR}/triplex_search.summary