#!/bin/bash

source constants.sh

interval=$1

IN_DIR=${OUT_DATA_DIR}/enh_active
OUT_DIR=${IN_DIR}/active_regions
ALL_ENH_ACTIVE=${OUT_DATA_DIR}/enh_active/all_enh_active.bed

[ -d ${OUT_DIR} ] || mkdir ${OUT_DIR}
[ -e ${ALL_ENH_ACTIVE} ] && rm ${ALL_ENH_ACTIVE}

for enh_per_mirna in ${IN_DIR}/*enh_active.bed
do
	FILE_NAME=${enh_per_mirna##*/}
	mirna_id="$( cut -d '_' -f 1 <<< "$FILE_NAME" )"
	bedtools intersect -wa \
	-a ${OUT_DATA_DIR}/${mirna_id}_seed_pos_in_genome \
	-b ${enh_per_mirna} | sort -k1,1 -k2,2n | uniq | \
	awk -v interval=${interval} '{print $1"\t"($2-interval)"\t"($3+interval)}' \
		> ${OUT_DIR}/${mirna_id}_active_regions.bed
	# +- 14bp with awk before saving
done