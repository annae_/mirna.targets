#!/bin/bash

source constants.sh

ALN_PATH=${OUT_DATA_DIR}/lncrna_sites_in_genome/myalns.tab
OUT_DIR=${OUT_DATA_DIR}/lncrna_sites_in_genome/sep
ENHANCERS=${WORK_DIR}/enhancers.mactophages.Mtb.bed
ENH_WITH_LNCRNA_SITE_DIR=${OUT_DATA_DIR}/enh_with_lncRNA_site

[ -d ${OUT_DIR} ] || mkdir ${OUT_DIR}
[ -d ${ENH_WITH_LNCRNA_SITE_DIR} ] || mkdir ${ENH_WITH_LNCRNA_SITE_DIR}

awk -F'\t' -v out_dir="$OUT_DIR" '{print $2"\t"$3"\t"$3+$4"\t\t"$1"\t"$5"\t"$11 > out_dir"/"$7".bed"}' ${ALN_PATH}

for res_per_lncrna in ${OUT_DIR}/*.bed
do
	FILE_NAME=${res_per_lncrna##*/}
	lncrna_id="${FILE_NAME%.*}"
	echo ${lncrna_id}
	bedtools intersect -wa -a ${ENHANCERS} -b ${res_per_lncrna} | sort -k1,1 -k2,2n | uniq \
		> ${ENH_WITH_LNCRNA_SITE_DIR}/${lncrna_id}_enh_with_lncrna_site.bed
done

# echo "lncrna_id enhancers sites"
# for enh_with_site in ${ENH_WITH_LNCRNA_SITE_DIR}/*_enh_with_lncrna_site.bed
# do
# 	FILE_NAME=${enh_with_site##*/}
# 	lncrna_id="$( cut -d '_' -f 1 <<< "$FILE_NAME" )"
# 	echo ${lncrna_id} | tr '\n' ' '
# 	LC_NUMERIC="en_US.UTF-8" printf '%.*f\n' 2 \
# 		$(echo $(less ${enh_with_site} | wc -l)/$(less ${ENHANCERS} | wc -l) | bc -l) | tr '\n' ' '
# 	LC_NUMERIC="en_US.UTF-8" printf '%.*f\n' 3 \
# 		$(echo $(less ${enh_with_site} | wc -l)/ \
# 		$(less ${OUT_DIR}/${lncrna_id}.bed | wc -l) | bc -l)
	
# done