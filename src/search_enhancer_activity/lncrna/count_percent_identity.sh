#!/bin/bash

source constants.sh

IN_DIR=${OUT_DATA_DIR}/lncrna_sites_in_genome/sep
OUT_DIR=${OUT_DATA_DIR}/enh_with_lncRNA_site/percent_ident
ENHANCERS=${WORK_DIR}/enhancers.mactophages.Mtb.bed
ENH_WITH_LNCRNA_SITE_DIR=${OUT_DATA_DIR}/enh_with_lncRNA_site

[ -d ${OUT_DIR} ] || mkdir ${OUT_DIR}
[ -d ${OUT_DIR}/aln_chr_in_enh ] || mkdir ${OUT_DIR}/aln_chr_in_enh

for res_per_lncrna in ${IN_DIR}/*.bed
do
	FILE_NAME=${res_per_lncrna##*/}
	lncrna_id="${FILE_NAME%.*}"
	echo ${lncrna_id}
	bedtools intersect -wb -a ${ENHANCERS} -b ${res_per_lncrna} | sort -k1,1 -k2,2n | uniq \
		> ${OUT_DIR}/aln_chr_in_enh/${lncrna_id}_aln_chr_in_enh.bed
	awk -F'\t' -v out_dir="$OUT_DIR" -v lncrna_id="$lncrna_id" '{print $1"\t"$2"\t"$3"\t"($3-$2)/$10 > out_dir"/"lncrna_id".bed"}' ${OUT_DIR}/aln_chr_in_enh/${lncrna_id}_aln_chr_in_enh.bed
done