#!/bin/bash

source constants.sh

LASTDB=${WORK_DIR}/db/unmasked/mm9.db
LNC_SEQ_PATH=${WORK_DIR}/lncrnas/DE.lncRNAs.fasta
OUT_DIR=${OUT_DATA_DIR}/lncrna_sites_in_genome

[ -d ${OUT_DIR} ] || mkdir ${OUT_DIR}
lastal -f tab ${LASTDB} ${LNC_SEQ_PATH} > ${OUT_DIR}/myalns.tab

echo 'lastal finsished calculation'