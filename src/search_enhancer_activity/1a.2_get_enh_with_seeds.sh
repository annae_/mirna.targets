#!/bin/bash

source constants.sh

ENH_WITH_SEED_DIR=${OUT_DATA_DIR}/enh_with_seeds
ENHANCERS=${WORK_DIR}/enhancers.mactophages.Mtb.bed

[ -d ${ENH_WITH_SEED_DIR} ] || mkdir ${ENH_WITH_SEED_DIR}

for res_per_mirna in ${OUT_DATA_DIR}/*seed_pos_in_genome
do
	FILE_NAME=${res_per_mirna##*/}
	mirna_id="$( cut -d '_' -f 1 <<< "$FILE_NAME" )"
	echo ${mirna_id}
	bedtools intersect -wa -a ${ENHANCERS} -b ${res_per_mirna} | sort -k1,1 -k2,2n | uniq \
		> ${ENH_WITH_SEED_DIR}/${mirna_id}_enh_with_seeds.bed
done


echo "mirna_id enhancers seeds"
for enh_with_seed in ${ENH_WITH_SEED_DIR}/*enh_with_seeds.bed
do
	FILE_NAME=${enh_with_seed##*/}
	mirna_id="$( cut -d '_' -f 1 <<< "$FILE_NAME" )"
	echo ${mirna_id} | tr '\n' ' '
	LC_NUMERIC="en_US.UTF-8" printf '%.*f\n' 2 \
		$(echo $(less ${enh_with_seed} | wc -l)/$(less ${ENHANCERS} | wc -l) | bc -l) | tr '\n' ' '
	LC_NUMERIC="en_US.UTF-8" printf '%.*f\n' 3 \
		$(echo $(less ${enh_with_seed} | wc -l)/ \
		$(less ${OUT_DATA_DIR}/${mirna_id}_seed_pos_in_genome | wc -l) | bc -l)
	
done