#!/bin/bash

source constants.sh

ENH_PATH=${WORK_DIR}/enhancers.mactophages.Mtb.mm9.fasta
MIR_SEQ_PATH=${WORK_DIR}/mirnas/DE_mirna_mature_seqs_M1.fa
OUT_DIR=${OUT_DATA_DIR}/enh_miranda

[ -d ${OUT_DIR} ] || mkdir ${OUT_DIR}

miranda ${MIR_SEQ_PATH} ${ENH_PATH} > ${OUT_DIR}/miranda.out
echo 'miRanda finsished calculation'