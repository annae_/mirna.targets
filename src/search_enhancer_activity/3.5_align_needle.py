# call as python name ${WORK_DIR} ${OUT_DATA_DIR}

from Bio.Emboss.Applications import NeedleCommandline
from Bio import SeqIO
import os, shutil,sys, pandas as pd

# work_dir = '/mnt/storage2/annae/macrophages/genome'
# # work_dir = '/home/anna/biengi/genome'
# out_data_dir = os.path.join(work_dir, 'out')

work_dir = sys.argv[1]
out_data_dir = sys.argv[2]
mirna_extent = sys.argv[3]
type_macrophages = sys.argv[4]

fasta_dir = os.path.join(out_data_dir, 'enh_active/active_regions/fasta')
fastas_sorted_dir = os.path.join(fasta_dir, 'fasta_sorted/')
fasta_sorted_one_per_file_dir = os.path.join(fasta_dir, 'one_per_file')
tools_dir = os.path.join(work_dir, 'tools')
alignments_dir = os.path.join(out_data_dir, 'alignments_needle')

print(mirna_extent)
if mirna_extent == 'mature':
    mirnas = os.path.join(work_dir, 'mirnas/mature_seqs_'+type_macrophages)
elif mirna_extent == 'seed':
    mirnas = os.path.join(work_dir, 'mirnas/seeds_fasta')


print 'Prepare all outputs as separate fasta files'

for fasta_sorted in os.listdir(fastas_sorted_dir):
    #print fasta_sorted
    fasta_sorted_one_per_file = os.path.join(fasta_sorted_one_per_file_dir, fasta_sorted)
    fasta_sorted_path = os.path.join(fastas_sorted_dir, fasta_sorted)
    #print fasta_sorted_one_per_file
    if os.path.exists(fasta_sorted_one_per_file) and os.path.isdir(fasta_sorted_one_per_file):
        shutil.rmtree(fasta_sorted_one_per_file)
    os.makedirs(fasta_sorted_one_per_file)
    # change so that no error with directory
    for record in SeqIO.parse(fasta_sorted_path, "fasta"):
        new_file = os.path.join(fasta_sorted_one_per_file, fasta_sorted + '_' + record.id.replace(':', '_'))
        #print new_file
        
        output = open(new_file,'w') 
        output.write(">%s\n%s\n"%(record.id, record.seq))
        output.close()



print 'perform alignments each mirna vs each of possibly active regions'

if os.path.exists(alignments_dir) and os.path.isdir(alignments_dir):
        shutil.rmtree(alignments_dir)
os.makedirs(alignments_dir)
    
for mirna in os.listdir(fasta_sorted_one_per_file_dir):
    print mirna
    mature_mirna_path = os.path.join(mirnas, mirna)
    active_regions_path = os.path.join(fasta_sorted_one_per_file_dir, mirna)
    out_alignments_per_mirna_dir = os.path.join(alignments_dir, mirna)
    
    if os.path.exists(out_alignments_per_mirna_dir) and os.path.isdir(out_alignments_per_mirna_dir):
        shutil.rmtree(out_alignments_per_mirna_dir)
    os.makedirs(out_alignments_per_mirna_dir)
    
    for active_region in os.listdir(active_regions_path):
        active_region_path = os.path.join(active_regions_path, active_region)
        # print active_region_path
        
        out_alignment=os.path.join(out_alignments_per_mirna_dir, active_region + "_needle.txt")
        needle_cline = NeedleCommandline(asequence=mature_mirna_path, 
                                         bsequence=active_region_path,
                                         gapopen=50, gapextend=0.5, 
                                         datafile='EBLOSUM62',
                                         outfile=out_alignment)
        stdout, stderr = needle_cline()



print 'extract percent identities from outputs'




def get_num_ident_nucl(out_alignment):
    with open(out_alignment) as f:
        for i, line in enumerate(f):
#             if i == 24:
#                 return(line.split()[-1].replace('(', '').replace(')', '')[:-1])
            if i == 25:
                num_ident_nucl = int(line.split('/')[0].split()[2])
    return(num_ident_nucl)


def get_mirna_length(mirna_path):
    for record in SeqIO.parse(mirna_path, "fasta"):
        return(len(record.seq))


def get_percent_identity(alignment_path, mirna_path):
    res = float(get_num_ident_nucl(alignment_path))/get_mirna_length(mirna_path)
    return(round(res,2))


PI_df = pd.DataFrame()
for mirna in os.listdir(alignments_dir):
#     print mirna
    mature_mirna_path = os.path.join(mirnas, mirna)
    alignments_per_mirna_path = os.path.join(alignments_dir, mirna)
    for active_region_aligned in os.listdir(alignments_per_mirna_path):
        active_region_chr_corr = ':'.join(active_region_aligned.replace('rev_compl_', '').split('_')[1:3])
        active_region_aligned_path = os.path.join(alignments_per_mirna_path, active_region_aligned)
        #print active_region_path
        PI = get_percent_identity(active_region_aligned_path,
                                  mature_mirna_path)
        #print PI
        PI_new = pd.DataFrame({'mirna': [mirna],
                               'active region': [active_region_chr_corr],
                               'PI': [PI]
                              })
        PI_df = PI_df.append(PI_new, ignore_index = True)
#print type(PI_df.iloc[1, 0])
PI_df = PI_df.sort_values(by=['PI'], ascending=False)
print PI_df
PI_df.to_csv(os.path.join(alignments_dir, 'stats.csv'), 
             sep = '\t',
             index = False)
