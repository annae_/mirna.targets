#!/bin/bash

OUT_PATH=/mnt/storage2/annae/macrophages/mirna.targets/out/mirwalk_res/2tools/lengths_24.txt

while IFS= read -r line; do
  echo "$line " | tr -d '\n' >> $OUT_PATH
  less /mnt/storage2/annae/macrophages/rna.gbk | grep -F -m 1 "LOCUS       $line " | sed -n "s/^.*\s* \(\S* bp\).*$/\1/p" | sed 's/ bp.*//' >> $OUT_PATH
done < "/mnt/storage2/annae/macrophages/mirna.targets/out/mirwalk_res/2tools/ncbi_accessions_24"