source('src/functions.R')
library(dplyr)
library(stringr)

gene.expr.path <- file.path(getwd(), '../expr.mouse.macrophage.minTPM10.annot.xlsx')
columns_to_leave <-  c(1, 2, 7:20, 24, 25, 27)
gene.expr.all <- data.table(read.xlsx(gene.expr.path, sheet = 1, startRow = 1,
                                        colNames = TRUE))[, ..columns_to_leave]
gene.expr.all[, ('Symbol') := colsplit(gene.expr.all$short_description, pattern='@', 
                                       names = c('pos', 'Symbol'))[2]]
colnames(gene.expr.all)[3:19] <- c('4.rep1', '4.rep2', '4.rep4',
                                     '12.rep1', '12.rep2', '12.rep4',
                                     '24.rep1', '24.rep2', '24.rep4',
                                     '48.rep1', '48.rep4',
                                     '96.rep1', '96.rep2', '96.rep4',
                                     '0.rep1', '0.rep2', '0.rep4')
gene.expr.all <- gene.expr.all[,c(1,2,20,17,18,19,3:16)]

gene.expr.all[, c('chr', 'pos') := colsplit(gene.expr.all$Annotation, pattern="\\:",  
                                              names = c('chr', 'pos'))]
gene.expr.all[, c('pos', 'strand') := colsplit(gene.expr.all$pos, pattern="\\,",  
                                            names = c('pos', 'strand'))]
gene.expr.all[, c('start', 'end') := colsplit(gene.expr.all$pos, pattern="\\..",  
                                       names = c('start', 'end'))]
gene.expr.all <- gene.expr.all[, c(2:3, 21, 23:25, 4:20)]

# duplicate expression rows for genes named with several possiible ways for each gene name separately
gene.names.variants <- unlist(lapply(tstrsplit(str_subset(gene.expr.all$Symbol, "@"), ",", fixed = TRUE), 
                      function (x) x[!is.na(x)]))
gene.names.variants[gene.names.variants %in% str_subset(gene.names.variants, "@")] <- 
  tstrsplit(str_subset(gene.names.variants, "@"), "@", fixed = TRUE)[[2]]
gene.names.variants <- unique(gene.names.variants)

subset.cut <- rbindlist(lapply(gene.names.variants, function(gene){
  dt <- gene.expr.all[str_which(gene.expr.all$Symbol, gene),]
  dt[, 'Symbol'] <- gene
  dt
}))

gene.expr.all <- gene.expr.all[!str_detect(gene.expr.all$Symbol, '@'),]
gene.expr.all <- rbindlist(list(gene.expr.all, subset.cut))

path <- file.path(getwd(), 'out/gene_expr_all.txt')
write.table(gene.expr.all, 
            path, 
            quote = F, 
            row.names = F, 
            col.names = T, 
            sep = '\t')



##################################################################
###########    make input for GSEA    ############################
###########       all DE targets      ############################
##################################################################
out.path <- file.path(getwd(), 'out', 'gsea_input')

######## 0 vs 4h
## table with replicates for 0vs4 comparison
gene.expr.0vs4 <- right_join(x = gene.expr.0, 
                             y = gene.expr.4,
                             by = c('short_description', 'Symbol'))
colnames(gene.expr.0vs4)[c(1,2)] <- c('Name', 'Description')


## check if there are degs which are absent in gene expression table and leave 
## among target degs only those which are present in gene expr table
path.degs.04 <- file.path(getwd(), 'out', 'mirwalk_res', '4h_targets_deg_3tools')
degs.04 <- fread(path.degs.04, header = FALSE)
degs.04.present.in.ge <- gene.expr.0vs4[gene.expr.0vs4$Description %in% degs.04$V1,]$Name
degs.04.present.in.ge.symb <- degs.04[degs.04$V1 %in% gene.expr.0vs4$Description]$V1

## make .gmx file for gene set based on positions+gene symbols
gmx <- data.table(degs.04.crossvalid.present.in.ge = c(NA, degs.04.present.in.ge))
path.degs.04.present.in.ge <- file.path(out.path, 
                                        '0vs4_targets_deg_3tools_present_in_ge.gmx')
write.table(gmx, 
            path.degs.04.present.in.ge, 
            quote = F, row.names = F, 
            col.names = T, 
            sep = '\t')


## make .gmx file for gene set based only on gene symbols
gmx.symb <- data.table(degs.04.crossvalid.present.in.ge = c(NA, degs.04.present.in.ge.symb))
path.degs.04.present.in.ge.symb <- file.path(out.path, 
                                        '0vs4_targets_deg_3tools_present_in_ge_symb.gmx')
write.table(gmx.symb, 
            path.degs.04.present.in.ge.symb, 
            quote = F, row.names = F, 
            col.names = T, 
            sep = '\t')


# gene annotations
gene.anno <- gene.expr.0vs4[,c(1,2)]
colnames(gene.anno) <- c('Probe Set ID', 'Gene Symbol')
gene.anno['Gene Title'] <- NA
write.table(gene.anno, 
            file.path(out.path, 'gene.anno.chip'), 
            quote = F, row.names = F, 
            col.names = T, 
            sep = '\t')


# format for GSEA gene expression matrix
# gene.expr.0vs4$Description <- NA
# write.table(gene.expr.0vs4, 
#             file.path(out.path, '0vs4.gene.expr.reps.txt'), 
#             quote = F, row.names = F, 
#             col.names = T, 
#             sep = '\t')







######## 0 vs 12h
## table with replicates for 0vs4 comparison
gene.expr.0vs12 <- right_join(x = gene.expr.0, 
                             y = gene.expr.12,
                             by = c('short_description', 'Symbol'))
colnames(gene.expr.0vs12)[c(1,2)] <- c('Name', 'Description')


## check if there are degs which are absent in gene expression table and leave 
## among target degs only those which are present in gene expr table
path.degs.012 <- file.path(getwd(), 'out', 'mirwalk_res', '12h_targets_deg_3tools')
degs.012 <- fread(path.degs.012, header = FALSE)
degs.012.present.in.ge <- gene.expr.0vs12[gene.expr.0vs12$Description %in% degs.012$V1,]$Name
degs.012.present.in.ge.symb <- degs.012[degs.012$V1 %in% gene.expr.0vs12$Description]$V1

## make .gmx file for gene set based on positions+gene symbols
gmx <- data.table(degs.012.crossvalid.present.in.ge = c(NA, degs.012.present.in.ge))
path.degs.012.present.in.ge <- file.path(out.path, 
                                        '0vs12_targets_deg_3tools_present_in_ge.gmx')
write.table(gmx, 
            path.degs.012.present.in.ge, 
            quote = F, row.names = F, 
            col.names = T, 
            sep = '\t')


## make .gmx file for gene set based only on gene symbols
gmx.symb <- data.table(degs.012.crossvalid.present.in.ge = c(NA, degs.012.present.in.ge.symb))
path.degs.012.present.in.ge.symb <- file.path(out.path, 
                                             '0vs12_targets_deg_3tools_present_in_ge_symb.gmx')
write.table(gmx.symb, 
            path.degs.012.present.in.ge.symb, 
            quote = F, row.names = F, 
            col.names = T, 
            sep = '\t')

# format for GSEA gene expression matrix
# gene.expr.0vs12$Description <- NA
# write.table(gene.expr.0vs12, 
#             file.path(out.path, '0vs12.gene.expr.reps.txt'), 
#             quote = F, row.names = F, 
#             col.names = T, 
#             sep = '\t')




######## 0 vs 24h
## table with replicates for 0vs4 comparison
gene.expr.0vs24 <- right_join(x = gene.expr.0, 
                              y = gene.expr.24,
                              by = c('short_description', 'Symbol'))
colnames(gene.expr.0vs24)[c(1,2)] <- c('Name', 'Description')


## check if there are degs which are absent in gene expression table and leave 
## among target degs only those which are present in gene expr table
path.degs.024 <- file.path(getwd(), 'out', 'mirwalk_res', '24h_targets_deg_3tools')
degs.024 <- fread(path.degs.024, header = FALSE)
degs.024.present.in.ge <- gene.expr.0vs24[gene.expr.0vs24$Description %in% degs.024$V1,]$Name
degs.024.present.in.ge.symb <- degs.024[degs.024$V1 %in% gene.expr.0vs24$Description]$V1

## make .gmx file for gene set based on positions+gene symbols
gmx <- data.table(degs.024.crossvalid.present.in.ge = c(NA, degs.024.present.in.ge))
path.degs.024.present.in.ge <- file.path(out.path, 
                                         '0vs24_targets_deg_3tools_present_in_ge.gmx')
write.table(gmx, 
            path.degs.024.present.in.ge, 
            quote = F, row.names = F, 
            col.names = T, 
            sep = '\t')


## make .gmx file for gene set based only on gene symbols
gmx.symb <- data.table(degs.024.crossvalid.present.in.ge = c(NA, degs.024.present.in.ge.symb))
path.degs.024.present.in.ge.symb <- file.path(out.path, 
                                              '0vs24_targets_deg_3tools_present_in_ge_symb.gmx')
write.table(gmx.symb, 
            path.degs.024.present.in.ge.symb, 
            quote = F, row.names = F, 
            col.names = T, 
            sep = '\t')

# format for GSEA gene expression matrix
# gene.expr.0vs24$Description <- NA
# write.table(gene.expr.0vs24, 
#             file.path(out.path, '0vs24.gene.expr.reps.txt'), 
#             quote = F, row.names = F, 
#             col.names = T, 
#             sep = '\t')



##################################################################
###########    make input for GSEA    ############################
###########    DE targets for dif miRNA  #########################
##################################################################
in.dir <- '/home/anna/data/macrophages/mirwalk_results/3utr'

## 0 vs 4 hours
targets.4 <- fread(file.path(in.dir, '4h_mirwalk_cross_valid'))[,c('mirnaid', 'genesymbol')]
targets.4 <- split(targets.4, targets.4$mirnaid)
targets.4.present.in.ge <- 
  lapply(names(targets.4), function(mirna){
    targets <- targets.4[[mirna]]$genesymbol
    ## check if there are degs which are absent in gene expression table and leave 
    ## among target degs only those which are present in gene expr table
    targets.04.present.in.ge <- gene.expr.0vs4[gene.expr.0vs4$Description %in% targets,]$Name
    dt <- data.table(mirna = targets.04.present.in.ge)
    colnames(dt) <- mirna
    empty.line <- data.table(NA)
    colnames(empty.line) <- mirna
    dt <- rbind(empty.line, dt)
    max.len <- 83
    if (length(dt[[1]])<max.len){
      new.dt <- data.table(rep(NA, max.len-length(dt[[1]]))-1)
      rownames(new.dt) <- c((length(dt[[1]])+1):max.len)
      names(new.dt) <- mirna
      dt <- rbind(dt, new.dt)
    }
    dt
})
names(targets.4.present.in.ge) <- names(targets.4)
targets.4.present.in.ge <- as.data.table(targets.4.present.in.ge)
write.table(targets.4.present.in.ge, 
            file.path(out.path, '0vs4.targets.mirnas.sep.gmx'), 
            quote = F, row.names = F, 
            col.names = T, 
            sep = '\t')


## 0 vs 12 hours
targets.12 <- fread(file.path(in.dir, '12h_mirwalk_cross_valid'))[,c('mirnaid', 'genesymbol')]
targets.12 <- split(targets.12, targets.12$mirnaid)
targets.12.present.in.ge <- 
  lapply(names(targets.12), function(mirna){
    targets <- targets.12[[mirna]]$genesymbol
    ## check if there are degs which are absent in gene expression table and leave 
    ## among target degs only those which are present in gene expr table
    targets.012.present.in.ge <- gene.expr.0vs12[gene.expr.0vs12$Description %in% targets,]$Name
    dt <- data.table(mirna = targets.012.present.in.ge)
    colnames(dt) <- mirna
    empty.line <- data.table(NA)
    colnames(empty.line) <- mirna
    dt <- rbind(empty.line, dt)
    max.len <- 22
    if (length(dt[[1]])<max.len){
      new.dt <- data.table(rep(NA, max.len-length(dt[[1]]))-1)
      rownames(new.dt) <- c((length(dt[[1]])+1):max.len)
      names(new.dt) <- mirna
      dt <- rbind(dt, new.dt)
    }
    dt
  })
names(targets.12.present.in.ge) <- names(targets.12)
targets.12.present.in.ge <- as.data.table(targets.12.present.in.ge)
write.table(targets.12.present.in.ge, 
            file.path(out.path, '0vs12.targets.mirnas.sep.gmx'), 
            quote = F, row.names = F, 
            col.names = T, 
            sep = '\t')


## 0 vs 24 hours
targets.24 <- fread(file.path(in.dir, '24h_mirwalk_cross_valid'))[,c('mirnaid', 'genesymbol')]
targets.24 <- split(targets.24, targets.24$mirnaid)
targets.24.present.in.ge <- 
  lapply(names(targets.24), function(mirna){
    targets <- targets.24[[mirna]]$genesymbol
    ## check if there are degs which are absent in gene expression table and leave 
    ## among target degs only those which are present in gene expr table
    targets.024.present.in.ge <- gene.expr.0vs24[gene.expr.0vs24$Description %in% targets,]$Name
    dt <- data.table(mirna = targets.024.present.in.ge)
    colnames(dt) <- mirna
    empty.line <- data.table(NA)
    colnames(empty.line) <- mirna
    dt <- rbind(empty.line, dt)
    max.len <- 83
    if (length(dt[[1]])<max.len){
      new.dt <- data.table(rep(NA, max.len-length(dt[[1]]))-1)
      rownames(new.dt) <- c((length(dt[[1]])+1):max.len)
      names(new.dt) <- mirna
      dt <- rbind(dt, new.dt)
    }
    dt
  })
names(targets.24.present.in.ge) <- names(targets.24)
targets.24.present.in.ge <- as.data.table(targets.24.present.in.ge)
write.table(targets.24.present.in.ge, 
            file.path(out.path, '0vs24.targets.mirnas.sep.gmx'), 
            quote = F, row.names = F, 
            col.names = T, 
            sep = '\t')



### let's save lists of degs targets which are present in ge
list <- gene.expr.0vs4[gene.expr.0vs4$Description %in% degs.04$V1,]$Description
path <- '/home/anna/data/macrophages/mirna.targets/out/mirwalk_res'
write.table(targets.24.present.in.ge, 
            file.path(path, '0vs4.targets.mirnas.sep.gmx'), 
            quote = F, row.names = F, 
            col.names = T, 
            sep = '\t')
