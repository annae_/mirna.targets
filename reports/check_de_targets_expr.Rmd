---
title: "Explratory analysis of DE miRNAs and threir DE targets gene expression levels"
author: "Elizarova Anna"
date: "10 August 2018"
output: pdf_document
params:
  out.dir.rend:
    value: out
  hists.ge:
    value: 0
  summary.all.reps:
    value: 0
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown

logfc graphs

```{r , echo=FALSE, fig.width=5, fig.height=5}
knitr::include_graphics('/home/anna/data/macrophages/mirna.targets/out/degs4.logfc.png')
knitr::include_graphics('/home/anna/data/macrophages/mirna.targets/out/degs12.logfc.png')
knitr::include_graphics('/home/anna/data/macrophages/mirna.targets/out/degs24.logfc.png')
```

ge scatter plots
```{r , echo=FALSE, fig.width=5, fig.height=5}
knitr::include_graphics('/home/anna/data/macrophages/mirna.targets/out/ge4rep1.png')
knitr::include_graphics('/home/anna/data/macrophages/mirna.targets/out/ge4rep2.png')
knitr::include_graphics('/home/anna/data/macrophages/mirna.targets/out/ge4rep4.png')
```


hists
```{r , echo=FALSE, fig.width=5, fig.height=5}
params$hists.ge[[1]]
plot.new()
params$hists.ge[[2]]
plot.new()
params$hists.ge[[3]]
```

```{r , echo=FALSE}
params$summary.all.reps
```